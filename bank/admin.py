from django.contrib import admin
from bank.models import Fee, Expense

admin.site.register(Fee, list_display=('id', 'user', 'created_on', 'date', 'value'))
admin.site.register(Expense, list_display=('id', 'created_on', 'name', 'is_consumable', 'info'))
