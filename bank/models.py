from django.db import models
from django.db.models import CheckConstraint, Q

from accounts.models import User

from django.utils.timezone import now


class Fee(models.Model):
    id = models.AutoField
    user = models.ForeignKey(User, on_delete=models.SET(0), verbose_name='пользователь')
    created_on = models.DateTimeField(default=now, verbose_name='время записи')
    date = models.DateField(null=False, verbose_name='месяц за который оплата')
    # в функционале сделать чтобы число месяца было фиксированным (например 01)
    value = models.IntegerField(null=False, default=500)

    # todo не работает, разобраться почему (value должно быть больше 0)
    CheckConstraint(check=Q(value__gte=0), name='fee_value_gte_0')

    class Meta:
        verbose_name = 'ежемесячный взнос'
        verbose_name_plural = 'ежемесячные взносы'
        db_table = "fee"
        ordering = ['date', 'user']


class Expense(models.Model):
    id = models.AutoField
    created_on = models.DateTimeField(default=now, verbose_name='время записи')
    is_consumable = models.BooleanField(default=0)
    name = models.CharField(max_length=200, verbose_name='название расхода', null=False)
    info = models.TextField('дополнительная информация', null=True, blank=True)
    value = models.IntegerField(null=False, default=500)

    # todo не работает, разобраться почему (value должно быть больше 0)
    CheckConstraint(check=Q(value__gte=0), name='expense_value_gte_0')

    class Meta:
        verbose_name = 'покупка'
        verbose_name_plural = 'покупки'
        db_table = "expense"
        ordering = ['-created_on']
