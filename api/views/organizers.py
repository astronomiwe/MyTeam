from django.core.exceptions import ObjectDoesNotExist
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response

from rest_framework.views import APIView

from api.models import Organizer
from api.serializers import OrganizerSerializer

properties_for_docs = {
    'name': openapi.Schema(type=openapi.TYPE_STRING, description='название организатора'),
    'info': openapi.Schema(type=openapi.TYPE_STRING,
                           description='описание организатора')}


class OrganizerView(APIView):
    @swagger_auto_schema()
    def get(self, request):
        """получение списка организаторов"""
        organizers = Organizer.objects.all()
        serializer = OrganizerSerializer(organizers, many=True)
        return Response(status=200,
                        content_type="application/json; charset=UTF-8",
                        data={"total": organizers.count(),
                              "records": serializer.data})

    @swagger_auto_schema(request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties=properties_for_docs))
    def post(self, request):
        """создание нового организатора"""
        serializer = OrganizerSerializer(data=request.data)
        if serializer.is_valid():
            new_organizer = serializer.create(validated_data=serializer.validated_data)
            return Response(status=201, headers={"Location": f"api/organizers/{new_organizer.id}"})
        else:
            return Response(status=400)


class OrganizerByIdView(APIView):
    @swagger_auto_schema()
    def get(self, request, organizer_id):
        """получение организатора по id"""
        try:
            organizer = Organizer.objects.get(id=organizer_id)
            serializer = OrganizerSerializer(organizer, many=False)
            return Response(status=200,
                            content_type="application/json; charset=UTF-8",
                            data={"total": 1,
                                  "records": serializer.data})
        except ObjectDoesNotExist:
            return Response(status=404)


@swagger_auto_schema(request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties=properties_for_docs))
def put(self, request, organizer_id):
    """изменение организатора"""
    serializer = OrganizerSerializer(data=request.data, many=False)
    if serializer.is_valid():
        organizer = Organizer.objects.filter(id=organizer_id).first()
        serializer.update(instance=organizer, validated_data=serializer.validated_data)
        return Response(status=204)
    else:
        return Response(status=400)  # todo корректный ответ, если не удалось обновить


@swagger_auto_schema()
def delete(self, request, organizer_id):
    """удаление организатора"""
    organizer = Organizer.objects.filter(id=organizer_id).delete()
    if not organizer[0]:
        return Response(status=404)  # todo корректный ответ, если не удалось удалить
    return Response(status=204)
