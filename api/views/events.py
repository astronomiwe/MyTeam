from django.core.exceptions import ObjectDoesNotExist
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response

from rest_framework.views import APIView

from api.models import Event
from api.serializers import EventSerializer

properties_for_docs = {
    'name': openapi.Schema(type=openapi.TYPE_STRING, description='название мероприятия'),
    'date': openapi.Schema(type=openapi.TYPE_STRING, description='Дата в формате "2021-09-03"'),
    'organizer': openapi.Schema(type=openapi.TYPE_INTEGER, description='id организатора'),
    'type': openapi.Schema(type=openapi.TYPE_INTEGER, description='id типа мероприятия', ),
    'weight': openapi.Schema(type=openapi.TYPE_INTEGER,
                             description='"вес мероприятия" - 1 по умолчанию, 2 для двухсуточных игр'),
    'info': openapi.Schema(type=openapi.TYPE_STRING,
                           description='описание мероприятия')}


class EventView(APIView):
    @swagger_auto_schema()
    def get(self, request):
        """получение списка мероприятий"""
        events = Event.objects.all()
        serializer = EventSerializer(events, many=True)
        return Response(status=200,
                        content_type="application/json; charset=UTF-8",
                        data={"total": events.count(),
                              "records": serializer.data})

    @swagger_auto_schema(request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties=properties_for_docs))
    def post(self, request):
        """создание нового мероприятия"""
        serializer = EventSerializer(data=request.data)
        if serializer.is_valid():
            new_event = serializer.create(validated_data=serializer.validated_data)
            return Response(status=201, headers={"Location": f"api/events/{new_event.id}"})
        else:
            return Response(status=400)


class EventByIdView(APIView):
    @swagger_auto_schema()
    def get(self, request, event_id):
        """получение мероприятия по id"""
        try:
            event = Event.objects.get(id=event_id)
            serializer = EventSerializer(event, many=False)
            return Response(status=200,
                            content_type="application/json; charset=UTF-8",
                            data={"total": 1,
                                  "records": serializer.data})
        except ObjectDoesNotExist:
            return Response(status=404)

    @swagger_auto_schema(request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties=properties_for_docs))
    def put(self, request, event_id):
        """изменение мероприятия"""
        serializer = EventSerializer(data=request.data, many=False)
        if serializer.is_valid():
            event = Event.objects.filter(id=event_id).first()
            serializer.update(instance=event, validated_data=serializer.validated_data)
            return Response(status=204)
        else:
            return Response(status=400)  # todo корректный ответ, если не удалось обновить

    @swagger_auto_schema()
    def delete(self, request, event_id):
        """удаление мероприятия"""
        event = Event.objects.filter(id=event_id).delete()
        if not event[0]:
            return Response(status=404)  # todo корректный ответ, если не удалось удалить
        return Response(status=204)
