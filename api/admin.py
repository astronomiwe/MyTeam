from django.contrib import admin
from api.models import Organizer, EventType, Event, Attendance

admin.site.register(Organizer, list_display=('id', 'name', 'info'))
admin.site.register(EventType, list_display=('id', 'type'))
admin.site.register(Event, list_display=('id', 'date', 'type', 'organizer', 'name', 'info', 'weight'))
admin.site.register(Attendance, list_display=('id', 'event', 'user', 'status', 'created_on'))
