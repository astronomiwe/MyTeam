from django.urls import path

from api.views.events import EventByIdView, EventView
from api.views.organizers import OrganizerByIdView, OrganizerView

app_name = "api"

urlpatterns = [
    path('events/<int:event_id>/', EventByIdView.as_view()),
    path('events/', EventView.as_view()),
    path('organizers/<int:organizer_id>/', OrganizerByIdView.as_view()),
    path('organizers/', OrganizerView.as_view())
]
