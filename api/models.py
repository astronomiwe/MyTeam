from django.db import models

from enum import Enum
from django.utils.timezone import now

from accounts.models import User


class Organizer(models.Model):
    """организатор"""
    id = models.AutoField
    name = models.CharField(max_length=200, verbose_name='название', null=False, unique=True)
    info = models.TextField(null=True, verbose_name='описание', blank=True)

    class Meta:
        verbose_name = 'организатор'
        verbose_name_plural = 'организаторы'
        db_table = "organizer"
        ordering = ['name', 'id']


class EventType(models.Model):
    """тип мероприятия"""
    id = models.AutoField
    type = models.TextField(null=True, verbose_name='тип')

    class Meta:
        verbose_name = 'тип мероприятия'
        verbose_name_plural = 'типы мероприятий'
        db_table = "event_type"
        ordering = ['id']


class Event(models.Model):
    """мероприятие"""
    id = models.AutoField
    date = models.DateField(null=False, verbose_name='дата проведения')
    type = models.ForeignKey(EventType, on_delete=models.SET(0), verbose_name='тип')
    name = models.CharField(max_length=200, verbose_name='название', null=False)
    organizer = models.ForeignKey(Organizer, on_delete=models.SET(0), verbose_name='организатор')
    weight = models.IntegerField(null=False, default=1, verbose_name='"вес"',
                                 help_text='2 для двухсуточной игры, для остальных мероприятий 1. '
                                           'Мероприятия типа посиделок в баре не вносятся')
    info = models.TextField('информация', null=True, blank=True)

    models.UniqueConstraint(fields=['date', 'name'],
                            name='uix_event')

    class Meta:
        verbose_name = 'мероприятие'
        verbose_name_plural = 'мероприятия'
        db_table = "event"
        ordering = ['-date', 'name']


class Status(Enum):
    YES = 'пойду'
    NO = 'не пойду'
    VISITED = 'посетил'

    @classmethod
    def choices(cls):
        return ((status.name, status.value) for status in cls)


class Attendance(models.Model):
    """отметка о посещении мероприятия"""
    id = models.AutoField
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='пользователь')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, verbose_name='мероприятие')
    created_on = models.DateTimeField(default=now, verbose_name='время записи')
    status = models.CharField(max_length=50, default=Status.NO, choices=Status.choices(),
                              verbose_name='статус')

    class Meta:
        verbose_name = 'посещение'
        verbose_name_plural = 'посещения'
        db_table = "attendance"
        ordering = ['event', 'status', 'user']
