from django.contrib import admin
from accounts.models import User

admin.site.register(User, list_display=('id', 'public_id', 'username', 'nametape',
                                        'callsign', 'created_on', 'updated_on'))
