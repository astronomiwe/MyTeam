from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.timezone import now


class User(AbstractUser):
    """пользователи"""
    id = models.AutoField
    public_id = models.IntegerField(verbose_name='публичный id', null=True, blank=True)
    username = models.CharField(max_length=25, verbose_name='логин', null=False, unique=True)
    created_on = models.DateTimeField(default=now, verbose_name='дата и время создания')
    updated_on = models.DateTimeField(default=now, verbose_name='дата и время обновления')
    nametape = models.CharField(max_length=25, null=True, verbose_name='позывной', unique=True)
    callsign = models.CharField(max_length=5, null=True, verbose_name='идентификатор', unique=True)

    readonly_fields = ('created_on')

    def __str__(self):
        return self.nametape if self.nametape else self.username

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'
        db_table = "user"
        ordering = ['nametape']


# class UserRole(Enum):
#     """Enum для RoleType
#     один пользователь может сочетать в себе несколько ролей"""
#     GUEST = 'гость'  # all: GET, user: PUT password
#     RECRUIT = 'рекрут'  # attendance: POST status # user: PUT nametape
#     MEMBER = 'член команды'  # user: PUT callsign
#     PAYMASTER = 'казначей'  # таблицы еще не созданы
#     EVENTMASTER = 'редактор календаря'  # event: POST PUT DELETE
#     LEADER = 'командир команды'  #
#     ADMIN = 'администратор'  # all: POST, PUT, DELETE
#
#     @classmethod
#     def choices(cls):
#         return ((role.name, role.value) for role in cls)
#
#
# class RoleType(Group):
#     """типы ролей (уникальны), используются для разграничения прав"""
#     id = models.AutoField
#     type = models.CharField(max_length=50, default=UserRole.GUEST, choices=UserRole.choices(), unique=True)
#
#     def __repr__(self):
#         return self.type
#
#     class Meta:
#         verbose_name = 'роль'
#         verbose_name_plural = 'роли'
#         db_table = "role_type"
#         ordering = ['id']
#
#     readonly_fields = ('id', 'type')
#
#
# class RoleJournal(models.Model):
#     """присвоенная пользователю роль"""
#     id = models.AutoField
#     role = models.ForeignKey(RoleType, on_delete=models.SET(0), verbose_name='роль')
#     created_on = models.DateTimeField(default=now, verbose_name='дата и время присвоения')
#     user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='пользователь')
#
#     readonly_fields = ('created_on')
#
#     def __str__(self):
#         return self.role
#
#     class Meta:
#         verbose_name = 'присвоенная роль'
#         verbose_name_plural = 'присвоенные роли'
#         db_table = "role_journal"
#         ordering = ['user', '-created_on', 'role', '-id']
