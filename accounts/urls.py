from django.urls import path

from accounts.views.auth import Signup
from accounts.views.users import UserView, UserByIdView

app_name = "accounts"


urlpatterns = [path('users/<int:user_id>/', UserByIdView.as_view()),
               path('users/', UserView.as_view()),
               path('signup/', Signup.as_view())]