from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.serializers import UserSerializer

properties_for_docs = {
    'username': openapi.Schema(type=openapi.TYPE_STRING, description='имя пользователя'),
    'password': openapi.Schema(type=openapi.TYPE_STRING, description='пароль'),
    'nametape': openapi.Schema(type=openapi.TYPE_STRING, description='позывной'),
    'callsign': openapi.Schema(type=openapi.TYPE_STRING,
                               description='идентификатор вида DD.X или DDXX, где XX - цифры')}


class Signup(APIView):
    @swagger_auto_schema(request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties=properties_for_docs))
    def post(self, request):
        """регистрация нового пользователя"""
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.create(validated_data=serializer.validated_data)
            user.set_password(str(request.data.get('password')))
            user.save()
            return Response(status=201, headers={"Location": f"accounts/users/{user.id}"})
        else:
            return Response(status=400)


class Login(APIView):
    """вход пользователя, получение токена"""
    pass


class UpdatePassword(APIView):
    """обновление пароля текущего пользователя"""
    pass


class UpdateToken(APIView):
    """обновление токена текущего пользователя"""
    pass


class Logout(APIView):
    """выход текущего пользователя"""
    pass
