# from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response

from rest_framework.views import APIView

from accounts.models import User
from accounts.serializers import UserSerializer


class UserView(APIView):
    @swagger_auto_schema()
    def get(self, request):
        """получение списка пользователей"""
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(status=200,
                        content_type="application/json; charset=UTF-8",
                        data={"total": users.count(),
                              "records": serializer.data})


class UserByIdView(APIView):
    @swagger_auto_schema()
    def get(self, request, user_id):
        """получение пользователя по id"""
        user = User.objects.get(id=user_id)
        serializer = UserSerializer(user, many=False)
        return Response(status=200,
                        content_type="application/json; charset=UTF-8",
                        data={"total": 1,
                              "records": serializer.data})
